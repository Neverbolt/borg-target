FROM alpine:3

RUN apk add --update --no-cache borgbackup openssh
RUN echo "PasswordAuthentication no" >> /etc/ssh/sshd_config

RUN adduser -h /home/borg -s /bin/sh -D borg && sed -i 's/borg:!:/borg:*:/' /etc/shadow  # need to unlock the user
RUN printf "" > /etc/motd

ENTRYPOINT ["/entry.sh"]
EXPOSE 22

COPY entry.sh /
