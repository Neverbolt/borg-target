# Borg Target

This [Dockerfile](Dockerfile) is a simple setup to be used as remote [borg](https://borgbackup.readthedocs.io/) target.

It sets up a borg installation as well as an SSHD server and can be run as follows:

```bash
docker run -e "SSH_PUB_KEY=$(cat ~/.ssh/id_rsa.pub)" \
           -v "$(pwd)/config:/etc/ssh" \
           -v "$(pwd)/backups:/home/borg/backups" \
           -p 22:22 neverbolt/borg-target:latest
```

The Dockerfile is pushed under [`neverbolt/borg-target`](https://hub.docker.com/repository/docker/neverbolt/borg-target).

