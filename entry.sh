#!/bin/sh

if [ ! -f /etc/ssh/sshd_config ]; then
    printf "AllowTcpForwarding no\nGatewayPorts no\nX11Forwarding no\nPasswordAuthentication no" > /etc/ssh/sshd_config

    echo "--- Created sshd_config"
    cat /etc/ssh/sshd_config
fi

if [ ! -f /home/borg/.ssh/authorized_keys ] && [ ! x"${SSH_PUB_KEY}" == "x" ]; then
    mkdir -p /home/borg/.ssh
    echo $SSH_PUB_KEY > /home/borg/.ssh/authorized_keys
    chmod 700 /home/borg/.ssh
    chmod 600 /home/borg/.ssh/authorized_keys
    chown -R borg:borg /home/borg/.ssh

    echo "--- Created authorized keys for user borg"
    cat /home/borg/.ssh/authorized_keys
    echo ""
fi

chown borg:borg /home/borg/backups

ssh-keygen -A

echo "--- Starting sshd"
exec /usr/sbin/sshd -D -e "$@"
